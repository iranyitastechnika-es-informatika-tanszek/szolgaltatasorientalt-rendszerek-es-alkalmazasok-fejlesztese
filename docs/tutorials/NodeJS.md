# NodeJS tutorial

## Introduction

This document describes how to create a Hello World application built in NodeJS using MongoDB.
Make sure you have installed Visual Studio Code, NPM and MongoDB.

!!! tip
    Running MongoDB as a docker container is also a great solution.

    ```bash
    docker run --name mongodb -p 27017:27017 mongo
    ```

## Preparing the NodeJS environment

At first we need some global NodeJS packages so that we can generate and run our project.
Issue the following commands from a command prompt:

```bash
npm install --global typescript
```

This will install TypeScript.
TypeScript is an open-source, strongly typed superset language of JavaScript.
TypeScript is created by Microsoft to make JavaScript development scalable for large production applications.
TypeScript code is translated to JavaScript by the TypeScript compiler.

## Creating the NodeJS project

Open a command prompt and go into a folder in your file system that should contain the project folder of your node project (e.g. `/home/[user]/Projects`).

Create a new folder called node_hello, and enter this folder:

```bash
mkdir node_hello
cd node_hello
```

Initialize the NodeJS project:

```bash
npm init --yes
```

This command will create default a `package.json` file which contains the dependencies of our project.

Now we can install all our required dependencies:

```bash
npm install --save express mongoose
```

The modules should install without errors.
The following modules have been installed:

- **express**: allows implementing REST operations
- **mongoose**: the driver to connect to MongoDB

Also, we can install our development-only dependencies:

```bash
npm install --save-dev typescript @types/node @types/http-errors @types/express
```

The modules should install without errors.
The following modules have been installed:

- **typescript**: TypeScript language support
- **@types/node**: TypeScript type descriptors for NodeJS
- **@types/http-errors**: TypeScript type descriptors for http-errors
- **@types/express**: TypeScript type descriptors for express

Now we can initialize TypeScript development:

```bash
tsc --init
```

This command will create default a `tsconfig.json` file which contains the configuration of the TypeScript compiler.

Create two folders, one for the TypeScript files (`src`) and one for the generated JavaScript files (`app`):

```bash
mkdir src
mkdir app
```

Open the `tsconfig.json` file in a text editor and modify the following highlighted lines to configure these

```json title="tsconfig.json" hl_lines="12 17 18"
{
  "compilerOptions": {
    /* Visit https://aka.ms/tsconfig to read more about this file */

    /* Projects */

    /* Language and Environment */
    "target": "es2016", /* Set the JavaScript language version for emitted JavaScript and include compatible library 

    /* Modules */
    "module": "commonjs", /* Specify what module code is generated. */
    "rootDir": "./src/", /* Specify the root folder within your source files. */

    /* JavaScript Support */

    /* Emit */
    "sourceMap": true, /* Create source map files for emitted JavaScript files. */
    "outDir": "./app/", /* Specify an output folder for all emitted files. */

    /* Interop Constraints */
    "esModuleInterop": true, /* Emit additional JavaScript to ease support for importing CommonJS modules. This enables 'allowSyntheticDefaultImports' for type compatibility. */
    "forceConsistentCasingInFileNames": true, /* Ensure that casing is correct in imports. */

    /* Type Checking */
    "strict": true, /* Enable all strict type-checking options. */

    /* Completeness */
    "skipLibCheck": true /* Skip type checking all .d.ts files. */
  }
}
```

Now the project is ready for development.

## Opening the project in VSCode

Start **Visual Studio Code**.

```bash
code .
```

Create a new file called `app.ts` under the `src` folder (right click on the src folder and select New File...).
The content of this file should be:

```ts title="src/app.ts"
import express, { Application, Request, Response } from "express";

const app: Application = express();
const PORT: number = 3000;

app.use("/", (req: Request, res: Response): void => {
  res.send("Hello world!");
});

app.listen(PORT, (): void => {
  console.log("Listening on:", PORT);
});
```

We need the TypeScript compiler to continuously run in the background and automatically compile TypeScript files to JavaScript.
To start the TypeScript compiler, press **Ctrl+Shift+B**, and select the **tsc: watch** task.
This will immediately compile the existing **.ts** files in the project and will continue to do so whenever you change a **.ts** file.
The generated JavaScript code will appear in the `app` folder.
VSCode will also report any errors found by the TypeScript compiler.

Modify the `package.json` to specify the generated `app/app.js` JavaScript file as our main file:

```json title="package.json" hl_lines="5 8"
{
  "name": "node_hello",
  "version": "1.0.0",
  "description": "",
  "main": "app/app.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "node app/app.js"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "express": "^4.18.2",
    "mongoose": "^7.1.2"
  },
  "devDependencies": {
    "@types/express": "^4.17.17",
    "@types/http-errors": "^2.0.1",
    "@types/node": "^20.2.1",
    "typescript": "^5.0.4"
  }
}
```

Now we can start our application. Go back to the `app.ts` file and select **Debug / Start Debugging** from the menu or pressing **F5**.
Select **Node.js** as the debugger. The server will start on port 3000. You can test it in the browser:

- [http://localhost:3000/](http://localhost:3000/)

You can stop the server in VSCode by pressing **Shift+F5**.

You can set breakpoints in the TypeScript file by selecting **Debug / Toggle breakpoint** from the menu or pressing **F9**.
The execution will stop at that line when the server is started in debug mode (by pressing **F5**).
The keyboard shortcuts for debugging are similar to the ones in Visual Studio.

## Publishing a new route

Create a new folder called `services` under the `src` folder.
Create a file called `hello_service.ts` inside the `src/services` folder with the following content:

```ts title="src/services/hello_service.ts"
// Import express:
import express, { NextFunction, Request, Response } from "express";

// Create a new express router:
const router = express.Router();

interface RequestParams {}

interface ResponseBody {}

interface RequestBody {}

// Specify that 'name' is a query parameter of type 'string':
interface RequestQuery {
  name: string;
}

/* GET request: */
router.get(
  "/",
  (
    req: Request<RequestParams, ResponseBody, RequestBody, RequestQuery>,
    res: Response
  ) => {
    // Get the 'name' query param:
    const name = req.query.name;
    // Send response:
    res.send("Hello, " + name);
  }
);

// Export the router:
export default router;
```

Next, we need to wire this router into the main application’s router.
In order to do this, open the `src/app.ts` and add the following highlighted lines:

```ts title="src/app.ts" hl_lines="2 3 8 10"
import express, { Application, Request, Response } from "express";
import bodyParser from "body-parser";
import helloService from "./services/hello_service";

const app: Application = express();
const PORT: number = 3000;

app.use(bodyParser.json());

app.use("/hello", helloService);

app.use("/", (req: Request, res: Response): void => {
  res.send("Hello world!");
});

app.listen(PORT, (): void => {
  console.log("Listening on:", PORT);
});
```

Now every request to a URL starting with **/hello** will go to our **hello_service**.
Restart the application and test it in a browser:

- [http://localhost:3000/hello?name=Mario](http://localhost:3000/hello?name=Mario)

## Accessing MongoDB

We need an ORM mapping from TypeScript to MongoDB to be able to access objects stored in the database conveniently.

At first, we have to define the interfaces of the objects.
Create a new folder called `interfaces` under the `src` folder.
Create a file called `hello.ts` inside the `src/interfaces` folder with the following content:

```ts title="src/interfaces/hello.ts"
export interface IHello {
  name: string;
  message: string;
}
```

Next, we have to define the Mongoose mapping of this interface to MongoDB.
Create a new folder called `schemas` under the `src` folder.
Create a file called `hello.ts` inside the `src/schemas` folder with the following content (make sure to use the String type with capital 'S' here):

```ts title="src/schemas/hello.ts"
import mongoose, { Schema, Document } from "mongoose";
import { IHello } from "../interfaces/hello";

const HelloSchema = new Schema<IHello & Document>({
  name: String,
  message: String,
});

export const Hello = mongoose.model("Hello", HelloSchema, "Hello");
```

Now we need to call this mapping from the service.
Update the `src/services/hello_service.ts` with the highlighted lines:

```ts title="src/services/hello_service.ts" hl_lines="2-4 23 26 30-36"
// Import express:
import express, { NextFunction, Request, Response } from "express";
import { Hello } from "../schemas/hello";
import { NotFound } from "http-errors";

// Create a new express router:
const router = express.Router();

interface RequestParams {}

interface ResponseBody {}

interface RequestBody {}

// Specify that 'name' is a query parameter of type 'string':
interface RequestQuery {
  name: string;
}

/* GET request: */
router.get(
  "/",
  async (
    req: Request<RequestParams, ResponseBody, RequestBody, RequestQuery>,
    res: Response,
    next: NextFunction
  ) => {
    // Get the 'name' query param:
    const name = req.query.name;
    const hello = await Hello.findOne({ name: name }).exec();
    if (hello) {
      // Send response:
      res.send(hello.message);
    } else {
      return next(NotFound());
    }
  }
);

// Export the router:
export default router;
```

Finally, we have to connect to MongoDB in the application.
Modify the following highlighted lines in the `src/app.ts` file:

```ts title="src/app.ts" hl_lines="4 17-25"
import express, { Application, Request, Response } from "express";
import bodyParser from "body-parser";
import helloService from "./services/hello_service";
import mongoose from "mongoose";

const app: Application = express();
const PORT: number = 3000;

app.use(bodyParser.json());

app.use("/hello", helloService);

app.use("/", (req: Request, res: Response): void => {
  res.send("Hello world!");
});

mongoose
  .connect("mongodb://127.0.0.1:27017/hello")
  .then((res) => {
    console.log("Connected to MongoDB");
    app.listen(PORT, (): void => {
      console.log("Listening on:", PORT);
    });
  })
  .catch((err) => console.log(err));
```

Let’s add some data into the database.
Frist, install the [MongoDB for VS Code](https://marketplace.visualstudio.com/items?itemName=mongodb.mongodb-vscode) extension.
Then, create a new database called **hello** with a collection name **Hello**.
Finally, add two documents with the following content:

```json
{ "name": "Alice", "message": "Good morning, Alice!" }
```

```json
{ "name": "Bob", "message": "Good evening, Bob!" }
```

Now we can test our application in a browser:

- [http://localhost:3000/hello?name=Alice](http://localhost:3000/hello?name=Alice)
- [http://localhost:3000/hello?name=Bob](http://localhost:3000/hello?name=Bob)
- [http://localhost:3000/hello?name=Nobody](http://localhost:3000/hello?name=Nobody)
